function [p] = plotReplayResults(subFileName)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% load data
% subFileName = 'VEMTav95.dat'
data = importdata(subFileName);
vpno = data.textdata(1);
seno = data.data(2,1);
ipdata = data.data;

% define necessary vars
cond = ipdata(:, 4); % 1= clock & no fix. period, 2= clock & fix period, 3= loading bar and no fix. period 4= loading bar and fix. period, 5= reaction time,
feco = ipdata(:, 5); % 1 = before feedback, 2 = feedback, 3 = after feedback  
diff = ipdata(:,44);

opdata = horzcat( cond, feco, diff);
nbins = 10;

% draw plot
p= figure;
str = sprintf('Participant %s%i Results\n\n', vpno{1}, seno);
suptitle(str);
hold on
subplot(1,3,1);
hold on
% plot stuff
subdat =  opdata(opdata(:,2) == 1,:);
histogram(subdat(subdat(:,1) == 1,3),nbins);
histogram(subdat(subdat(:,1) == 2,3),nbins);
histogram(subdat(subdat(:,1) == 3,3),nbins);
histogram(subdat(subdat(:,1) == 4,3),nbins);
histogram(subdat(subdat(:,1) == 5,3),nbins);
title('Before feedback block')
xlabel('Difference between apperture shift and response [ms]');
ylabel('Frequency count');
legend('Clock & no fix.', 'Clock & fix.', 'Loading bar & no fix.', 'Loading bar & fix.', 'Reaction time');    
hold off

subplot(1,3,2);
hold on
% plot stuff
subdat =  opdata(opdata(:,2) == 2,:);
histogram(subdat(subdat(:,1) == 1,3),nbins);
histogram(subdat(subdat(:,1) == 2,3),nbins);
histogram(subdat(subdat(:,1) == 3,3),nbins);
histogram(subdat(subdat(:,1) == 4,3),nbins);
histogram(subdat(subdat(:,1) == 5,3),nbins);
title('Feedback block')
xlabel('Difference between apperture shift and response [ms]');
ylabel('Frequency count');
legend('Clock & no fix.', 'Clock & fix.', 'Loading bar & no fix.', 'Loading bar & fix.', 'Reaction time');    
hold off

subplot(1,3,3);
hold on
% plot stuff
subdat =  opdata(opdata(:,2) == 3,:);
histogram(subdat(subdat(:,1) == 1,3),nbins);
histogram(subdat(subdat(:,1) == 2,3),nbins);
histogram(subdat(subdat(:,1) == 3,3),nbins);
histogram(subdat(subdat(:,1) == 4,3),nbins);
histogram(subdat(subdat(:,1) == 5,3),nbins);
title('After feedback block')
xlabel('Difference between apperture shift and response [ms]');
ylabel('Frequency count');
legend('Clock & no fix.', 'Clock & fix.', 'Loading bar & no fix.', 'Loading bar & fix.', 'Reaction time');    
hold off
hold off

