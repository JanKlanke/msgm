%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Microsaccade parameter to Gabor patch parameter match %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2019 by Jan Klanke
%
% 
% DESKRIPTION
%
%
%
% TODOs
%
% 
%


clear all;
clear mex;

clear functions; 

addpath('Functions/','Data/');

home;
expStart=tic;

global setting visual scr keys re %#ok<*NUSED>

setting.TEST = 0;         % test in dummy mode? 0=with eyelink; 1=mouse as eye; 2=no gaze position checking of any sort
setting.sloMo= 1;         % To play stimulus in slow motion, draw every frame setting.sloMo times
setting.train= 0;         % do you want to run a pracitce block?
setting.Pixx = 0;         % Is this happening with the Propixx or some other kind of screen?

setting.stimC = 3;        % How many concentric circles of stimuli do you want?
setting.stimN = 8;        % How many Gabor patches per circle do you want?

exptname = 'MSGM';

try 
    
    newFile = 0;
    while ~newFile
        % get vp and session id
        [vpno, seno, subjectCode] = getVpCode(exptname);
        subjectCode = strcat('Data/',subjectCode);
        
        % get conditions to be tested and dom. eye
        % blor = getConditions;
        domEye = getDomEye;
        
        % create data file
        datFile = sprintf('%s.dat',subjectCode);
        if exist(datFile,'file')
            o = input('>>>> This file exists already. Should I overwrite it [y / n]? ','s');
            if strcmp(o,'y')
                newFile = 1;
            end
        else
            newFile = 1;
        end
    end
    
    % prepare screens
    prepScreen;
    
    % get key assignment
    getKeyAssignment;
    
    % disable keyboard
    ListenChar(2);
          
    % prepare stimuli
    prepStim;
    
    % generate design
    design = genDesign(vpno, seno, domEye, subjectCode);
    
    % initialize eyelink-connection
    if setting.TEST<2
        [el, err]=initEyelinkNew(subjectCode(6:end));
        if err==el.TERMINATE_KEY
            return
        end
    else
        el=[];
    end
    
    % runtrials
    design = runTrials(design,datFile,el);
    
    % shut down everything
    reddUp;
    
catch me
    rethrow(me);
    
    reddUp; %#ok<UNRCH>
end

expDur=toc(expStart);

fprintf(1,'\n\nThis (part of the) experiment took %.0f min.',(expDur)/60);
fprintf(1,'\n\nOK!\n');


% plotReplayResults(datFile);