function drawLoadingScreen(col, loc, siz, prct, windowptr)
%
% 2020 by Jan Klanke

global visual

location = loc + round(visual.ppd * [-siz -siz siz siz]);
Screen('FillArc', windowptr, col, location, 0, prct)
Screen('Flip', windowptr);