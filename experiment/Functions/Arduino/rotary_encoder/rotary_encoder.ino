
#define collector_a_pin 2
#define collector_b_pin 3
#define button_pin 7

volatile long ticks = 0;
volatile bool a_is_on;
volatile bool b_is_on;
volatile bool a_was_on;
volatile bool b_was_on;

void setup() {
  Serial.begin(115200);
  pinMode(collector_a_pin, INPUT_PULLUP);
  pinMode(collector_b_pin, INPUT_PULLUP);
  pinMode(button_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(collector_a_pin), encoder_changed, CHANGE);
  attachInterrupt(digitalPinToInterrupt(collector_b_pin), encoder_changed, CHANGE);
}

void loop() {
  if (Serial.available() > 0){
    int request = Serial.read();
    if (request == 1){
      Serial.println(String("") + ticks + " " + button_pressed()sa);
    }
    else if (request == 2){
      ticks = 0;
    }
  }
}

int button_pressed(){
  // with pullup resistor activated, button pressed is read as 0
  return -digitalRead(button_pin) + 1;
}

void encoder_changed(){
  a_is_on = digitalRead(collector_a_pin);
  b_is_on = digitalRead(collector_b_pin);

  ticks += parse_encoder();

  a_was_on = a_is_on;
  b_was_on = b_is_on;
}

int parse_encoder(){
  if (a_was_on && b_was_on) {
    if (!a_is_on && b_is_on) return 1;
    if (a_is_on && !b_is_on) return -1;
  }
  else if (!a_was_on && b_was_on) {
    if (!a_is_on && !b_is_on) return 1;
    if (a_is_on && b_is_on) return -1;
  }
  else if (!a_was_on && !b_was_on){
    if (a_is_on && !b_is_on) return 1;
    if (!a_is_on && b_is_on) return -1;
  }
  else if (a_was_on && !b_was_on){
    if (a_is_on && b_is_on) return 1;
    if (!a_is_on && !b_is_on) return -1;
  }
}

