function design = genDesign(vpno, seno, domEye, subjectCode)
%
% Notes on the design structure:
%
% General info:
%   .nTrain - number of training trials
%   .nTrial - number of test trial
%
% Trial info (.trial is replaced by .train in practice trials):
%   .trial(t).

global visual scr keys setting %#ok<NUSED>

% convenience wrapper function for easy pickings
paren = @(x, varargin) x(varargin{:});

% randomize random
rand('state',sum(100*clock));

% this subject's main sequence parameters
V0 = 450;A0 = 7.9;durPerDeg = 2.7;durInt = 23;

% participant parameters
design.vpno = vpno;
design.seno = str2num(seno);
design.domEye = domEye;

% condition parameter
design.nBlocks = 4;                 % number of blocks
design.nTrialsPerCellInBlock = 80;  % number of trials per cell in block
design.sacRequ = 0;                 % 0 = no saccade, 1 = saccade

% setting for fixation (before cue)
design.timFixD = 0.100;             % minimum fixation duration before stim onset [s]
design.timFixJ = 0.000;             % additional fixation duration jitter before cue onset [s]

% timing settings
design.timAfKe = 0.200;             % recording time after keypress  [s]
design.timMaSa = 1.000;             % time to make a saccade (most likely)  [s]
design.iti     = 0.000;             % inter-trial interval [s]

% onset settings for stimuli
design.timStil = 0.000;             % delay of stimulus onset (after fixation duration end) [s]
design.timStiD = design.timMaSa;    % stimulus stream duration    [s]

% fixation dot parameters 
design.fixSiz = 0.1;                % size of the fixation dot [dva]
design.gosSiz = 0.3;                % size of the fixation circle [dva]

% stimulus parameters
design.numStPC = setting.stimN;                   % number of stimuli per circle
design.numCirc = setting.stimC;                   % number of circles
design.numStim = design.numStPC * design.numCirc; % number of stimuli
[design.disStim, design.sizStim] = getGaborSizes; % distance of stimuli from scr center [dva]
design.oriStim = repmat([linspace(pi, pi/4, design.numStPC/2),linspace(2*pi, 5*pi/4, design.numStPC/2)], 1, design.numCirc);       % stimulus orientation (CCW) [rad]
design.plmStim = repmat(linspace(0, 2*pi - (2*pi) / design.numStPC, design.numStPC), 1, design.numCirc); % stimulus placement in cirle (CW) [rad]
design.posStim = [cos(design.plmStim) .* design.disStim; sin(design.plmStim) .* design.disStim];         % stimulus position  

V0= 100;A0=0.44;
amp = [1 .5 .1];    % amlitude of three ms onto which the phase shift velocity will be based see below
design.velStim = V0 * (1 - exp(-amp / A0));      % stimulus phase shift velocities based on the formula and values of the main sequence reported in Collewijn, 1988
design.minSpFr = 60 ./ design.velStim;                  % min spatial frequencies for velocities (so that they are invisible under stable fix.: Temporal frequency for invisible stimuli (ref. Castet 2002))
design.maxSpFr = 1 ./ (2 .* (design.velStim ./ 1440));  % max spatial frequencies for velocities (so as to prevent aliasing)

totTrials = design.nBlocks * design.nTrialsPerCellInBlock;
c = 0;
for b = 1:design.nBlocks
    t = 0;
    for itri = 1:design.nTrialsPerCellInBlock
        t = t + 1;
        c = c + 1;
        fprintf(1,'\n preparing trial %i ...',t);
        trial(t).trialNum = t; %#ok<*AGROW>

        % fixation positions
        fixx = 0;   % eccentricity of fixation x (relative to screen center) - can be pos or neg
        fixy = 0;   % eccentricity of fixation y (relative to screen center) - can be pos or neg
        stxx = design.posStim(1,:); 
        stxy = design.posStim(2,:);

        % temporal trial settings
        % duration before target onset [frames]
        trial(t).fixNFr = round((design.timFixD + design.timFixJ * rand) / scr.fd);
        trial(t).fixDur = trial(t).fixNFr * scr.fd;

        % duration after target onset [frames]
        trial(t).stiNFr = round((design.timStil + design.timStiD) / scr.fd);
        trial(t).stiDur = trial(t).stiNFr * scr.fd;

        % duration of stimulus contrast ram [frames]
        trial(t).ramNFr = round((design.timStiD / 5) / scr.fd);
        trial(t).ramDur = trial(t).ramNFr * scr.fd;

        % calculate total stimulus duration [frames]
        trial(t).totNFr = trial(t).fixNFr + trial(t).stiNFr;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % generate flag streams for stimulus presentation %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % time of the fixation            
        fixBeg = 1;
        fixEnd = trial(t).fixNFr;      

        % time that the stimulus is present
        stiBeg = trial(t).fixNFr + 1 + round(design.timStil / scr.fd);
        stiEnd = stiBeg + trial(t).stiNFr - 1;

        % time that the stimulus is at full contrast
        ctrBeg = stiBeg + trial(t).ramNFr;                              
        ctrEnd = stiEnd - trial(t).ramNFr; 

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %              spatial information                %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % fixation
        trial(t).fixa.vis = zeros(1,trial(t).totNFr);
        trial(t).fixa.vis(stiBeg:stiEnd) = 1;
        trial(t).fixa.loc = visual.scrCenter + round(visual.ppd * [fixx fixy fixx fixy]);
        trial(t).fixa.sin = round(design.fixSiz * visual.ppd);
        trial(t).fixa.sou = round(design.gosSiz * visual.ppd);
        trial(t).fixa.col = visual.black;
        
        % disruptor
        trial(t).disr.vis = zeros(1,trial(t).totNFr);
        trial(t).disr.vis(fixBeg:fixEnd) = 1;
        trial(t).disr.loc = visual.scrCenter + round(visual.ppd * [fixx fixy fixx fixy]);
        trial(t).disr.rad = repelem(sort(round(trial(t).fixa.sin + 10/2 * scr.ppd * rand(1,12)),'descend'),1,scr.rate);
        trial(t).disr.siz = trial(t).fixa.sou;
        trial(t).disr.col = visual.black;
        
        % determine stimulus parameters
        % spatial stimulus settings (standard)
        trial(t).stims.locX = visual.scrCenter(1) + round(visual.ppd * stxx);
        trial(t).stims.locY = visual.scrCenter(2) + round(visual.ppd * stxy);

        % determine stimulus parameters
        % spatial stimulus settings (standard)
        trial(t).stims.col  = repmat(visual.black, design.numStim, 1);
        trial(t).stims.pars = getGaborPars(design.numStim);
        trial(t).stims.pars.ori = rad2deg(design.oriStim); 
        trial(t).stims.pars.frq = repelem(ceil(design.minSpFr+1.2),1,design.numStPC);
        trial(t).stims.pars.frqp = trial(t).stims.pars.frq/visual.ppd;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %        internal/external velocity stuff         %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % define change in stimulus position across frames
        trial(t).posVec = zeros(2,trial(t).totNFr); 

        % define modulation of top velocity across frames
        % (here, we are keeping stimulus velocity constant)
        velVec = ones(1,length(stiBeg:stiEnd));
        trial(t).stims.tmpfrq = 0; 

        % changed from saccade dir. to movement 
        % dir because one can no longer infer the
        % saccade direction from  stimulus properties 
        % (as has been done originally)
        trial(t).velos = repelem(design.velStim,1,design.numStPC);
        trial(t).stims.evovel = repmat(velVec,design.numStim,1) .* trial(t).velos' + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec)); % desired stimulus velocity [dva per sec]

        % define phase change per frame for entire profile
        phaFra = scr.fd*trial(t).stims.evovel.*repmat(trial(t).stims.pars.frq'*360,1,length(velVec));  % phase change per frame [deg per fra]

        % define stimulus visibility and velocity
        trial(t).stims.vis = zeros(design.numStim,trial(t).totNFr);
        trial(t).stims.pha = zeros(design.numStim,trial(t).totNFr);
        trial(t).stims.vis(:,stiBeg:stiEnd) = 1;
        trial(t).stims.pha(:,stiBeg:stiEnd) = cumsum(phaFra,2);

        % add random phase to each stimulus
        trial(t).stims.pha = trial(t).stims.pha + repmat(360*rand(design.numStim,1),1,trial(t).totNFr);

        % define modulation of contrast across time
        ampVec = ones(1,length(stiBeg:stiEnd));
        ramVec = normcdf(1: trial(t).ramNFr, trial(t).ramNFr/2, trial(t).ramNFr/6);
        ampVec(1: trial(t).ramNFr) = ramVec;
        ampVec(end:-1:(end- trial(t).ramNFr+1)) = ramVec;
        trial(t).stims.evoamp = repmat(trial(t).stims.pars.amp',1,length(ampVec)) .* repmat(ampVec,design.numStim,1);
        trial(t).stims.amp = zeros(design.numStim,trial(t).totNFr);
        trial(t).stims.amp(:,stiBeg:stiEnd) = trial(t).stims.evoamp;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % define critical events during stimulus presentation %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        trial(t).events = zeros(1,trial(t).totNFr);
        trial(t).events(fixBeg) = 1;
        trial(t).events(fixEnd) = 2;
        trial(t).events(stiBeg) = 3;
        trial(t).events(ctrBeg) = 4;
        trial(t).events(ctrEnd) = 5;
        trial(t).events(stiEnd) = 6;

        % time requirements for responses
        trial(t).maxSac = design.timMaSa;
        trial(t).aftKey = design.timAfKe;

        % participant and trial info
        trial(t).vpno = design.vpno;
        trial(t).seno = design.seno;
        trial(t).block = b;
        trial(t).trial = t;
        trial(t).domEye = design.domEye;

        % store stimulus features
        trial(t).fixpox = fixx;     % x coordinate of position of fixation [dva rel. to midpoint]
        trial(t).fixpoy = fixy;     % y coordinate of position of fixation [dva rel. to midpoint] 
        trial(t).stiphad_ins = 1;   % direction of phase shift [leftward (-1), rightward (1)] of inner circ.
        trial(t).stiphad_mid = 1;   % direction of phase shift [leftward (-1), rightward (1)] of middle circ.
        trial(t).stiphad_out = 1;   % direction of phase shift [leftward (-1), rightward (1)] of outer circ.
        trial(t).stidist_ins = paren(unique(design.disStim),1); % distance of stimulus center and screen center, inner circ. [dva]
        trial(t).stidist_mid = paren(unique(design.disStim),2); % distance of stimulus center and screen center, middle circ. [dva]
        trial(t).stidist_out = paren(unique(design.disStim),3); % distance of stimulus center and screen center, outer circ. [dva]
        trial(t).stisize_ins = paren(unique(design.sizStim),1); % radius of stimulus patch, inner circ. [dva]
        trial(t).stisize_mid = paren(unique(design.sizStim),2); % radius of stimulus patch, middle circ. [dva]
        trial(t).stisize_out = paren(unique(design.sizStim),3); % radius of stimulus patch, outer circ. [dva]
        trial(t).stispfr_ins = paren(unique(trial(t).stims.pars.frq),1); % spatial frq. of stimulus patch, inner circ. [dva]
        trial(t).stispfr_mid = paren(unique(trial(t).stims.pars.frq),2); % spatial frq. of stimulus patch, middle circ. [dva]
        trial(t).stispfr_out = paren(unique(trial(t).stims.pars.frq),3); % spatial frq. of stimulus patch, outer circ. [dva]
        trial(t).stivelo_ins = design.velStim(3); % velocity of stimulus phase shift, inner circ. [dva/s]
        trial(t).stivelo_mid = design.velStim(2); % velocity of stimulus phase shift, middle circ. [dva/s]
        trial(t).stivelo_out = design.velStim(1); % velocity of stimulus phase shift, outer circ. [dva/s]
        
        % draw loading screen
        arc = linspace(1, 360, totTrials);
        drawLoadingScreen(visual.black, visual.scrCenter, 5, arc(c), scr.main);
    end
    r = randperm(t);             % randomise the trials per block
    if ~setting.train
        design.b(:,b).train = [];
        design.b(:,b).trial = trial(r);     % randomise in which order the condtions are presented (in experiment)
    elseif setting.train
        design.b(:,b).train = trial(r);     % randomise in which order the condtions are presented (in training)
        design.b(:,b).trial = [];
    end
end

design.nBlocks = b;                             % this number of blocks is shared by all qu
design.blockOrder = randperm(b);                % org: design.blockOrder = 1:b;

design.nTrialsPB = t;                           % number of trials per Block

save(sprintf('%s.mat',subjectCode),'design','visual','scr','keys','setting');