function [data,eyeData] = runSingleTrial(td)
%
% td = trial td
%
% 2019 by Jan Klanke

global scr visual setting keys

% clear keyboard buffer
FlushEvents('KeyDown');
pixx = setting.Pixx;
if ~setting.TEST == 1; HideCursor(); end 

% function for rounding away from zero
ceilfix = @(x)ceil(abs(x)) .* sign(x);

% predefine boundary information
cxm = td.fixa.loc(1);
cym = td.fixa.loc(2);
rad = visual.boundRad;
chk = visual.fixCkRad;

% draw trial information on operator screen
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm-chk)*2, (cym-chk)*2, (cxm+chk)*2, (cym+chk)*2); end

% generate Procedural Gabor textures
nStim = length(td.stims.pars.sizp);
sti.tex = repelem(visual.procGaborTex, 1, setting.stimN);
sti.vis = td.stims.pars.sizp;
sti.src = [zeros(2,nStim); sti.vis; sti.vis];
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX, td.stims.locY);
for f= 1:td.totNFr, stiFrames(f).dst = sti.dst + repmat(td.posVec(:,f),2,nStim); end

% predefine time stamps
tFixaOn = NaN;  % t of fixation on
tFixaOf = NaN;  % t of fixation on
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tStimCd = NaN;  % t of stimulus no longer at full contrast
tStimOf = NaN;  % t of stimulus off
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% eyeData
eyeData = [];

% Initialize vector important for response
pressed = 0; spaKey = 0;

% Initialize vector to store data on timing
frameTimes = NaN(1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while ~firstFlip
    if pixx, firstFlip = PsychProPixx('QueueImage', scr.myimg);
    else firstFlip = Screen('Flip', scr.myimg); end
end

f = 0;              % set frame count to 0
tLoopBeg = GetSecs; % get a first timestap
t = tLoopBeg;

while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    for slo = 1:setting.sloMo
        % Background
        Screen('FillRect', scr.myimg, visual.bgColor);
        % Disruptor
        if td.disr.vis(f)
            drawDisruptor(td.disr.col, td.disr.loc, td.disr.rad(f), td.disr.siz, scr.myimg);     
        end
        % Fixation
        if td.fixa.vis(f)
            drawFixation(td.fixa.col, td.fixa.loc, td.fixa.sin, scr.myimg);            
        end
        % Stimuli
        if td.stims.vis(:,f)
            Screen('DrawTextures', scr.myimg, sti.tex, sti.src, stiFrames(f).dst, td.stims.pars.ori', [], [], [], [], kPsychDontDoRotation, [td.stims.pha(:,f), td.stims.pars.frqp', td.stims.pars.sigp', td.stims.amp(:,f), td.stims.pars.asp', zeros(nStim,3)]');
        end 
        % Flip
        if pixx, nextFlip = PsychProPixx('QueueImage', scr.myimg);
        else nextFlip = Screen('Flip', scr.myimg); end 
        % screen2gif(f,sprintf('Data/gif/%s%02d%02d%02d_%04d.png',td.vpno,td.seno,td.block,td.trial,f)); 
    end
    frameTimes(f) = GetSecs;
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%

    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~setting.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
    if isnan(tFixaOf) && td.events(f)==2
        if ~setting.TEST  ; Eyelink('message', 'EVENT_FixaOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_FixaOf'); end
        tFixaOf = frameTimes(f);
    end
    if isnan(tStimOn) && td.events(f)==3
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==4
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==5
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==6
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if setting.TEST<2
        [x,y] = getCoord;
        if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
            fixBreak = 1;
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
    end
    t = GetSecs;
end

lastFlip = 0;
while ~lastFlip && ~nextFlip
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
    else lastFlip = Screen('Flip', scr.myimg); end 
end
tLoopEnd = GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation %
%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, PsychProPixx('QueueImage', scr.myimg);
    else Screen('Flip', scr.myimg); end
end
% Init rotary encoder, reset if necessary
% ticks = re.getStatus();
% if ticks ~= 0, re.resetEncoder(); end
WaitSecs(td.aftKey/4);

switch breakIt
    case 1
        data = 'fixBreak';
        if setting.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    otherwise
        % Snd('Play',[repmat(0.5,1,1050) linspace(0.5,0.0,50)].*[zeros(1,1000) sin(1:100)],5000);
        % nec. Parameters
        
        tResBeg = GetSecs;
        
        % set mouse position to screen center and show it
        SetMouse(scr.centerX, scr.centerY, scr.myimg); ShowCursor('CrossHair');
        while ~pressed
             
            % create response graphics for clock 
            for i = 1:scr.rate
                Screen('FillRect', scr.myimg, visual.bgColor);         % draw background
                if pixx, PsychProPixx('QueueImage', scr.myimg);
                else Screen('Flip', scr.myimg); end 
            end
            
            % get mouse input
            [resX,resY,pressed] = GetMouse(scr.main);
            
            % evaluate space bar and get response time
            if checkTarPress(keys.resSpace)
                pressed = 1; spaKey = 1;
            end
            tRes = GetSecs();                
            
        end
        tResDur = tRes - tResBeg;
        HideCursor;
        WaitSecs(td.aftKey);
        
        % evaluate results
        if spaKey
            resX = NaN; 
            resY = NaN;
            resX_dva = NaN;
            resY_dva = NaN;
        else
            resX = resX - scr.centerX;
            resY = resY - scr.centerY;
            resX_dva = pix2dva(resX, scr);
            resY_dva = pix2dva(resY, scr);
        end
            
        
        tClr = GetSecs;
        if setting.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if setting.TEST; fprintf(1,'\nEVENT_Clr'); end 
        
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        condData = sprintf('%i',...
            [setting.train]);            % in results, cells  6:9
        
        % collect stimulus information
        stimData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%i\t%i\t%i\t%.2f\t%.2f\t%.2f',...  % in results, cells 10:20
            [setting.stimC setting.stimN td.fixpox td.fixpoy td.stiphad_ins td.stiphad_mid td.stiphad_out td.stidist_ins td.stidist_mid td.stidist_out td.stisize_ins td.stisize_mid td.stisize_out td.stispfr_ins td.stispfr_mid td.stispfr_out td.stivelo_ins td.stivelo_mid td.stivelo_out]);
                     
        % collect time information 
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',...                              % in results, cells 27:36
            round(1000*([tFixaOn tFixaOf tStimOn tStimCf tStimCd tStimOf tRes tClr]-tStimOn)));
        
        % collect response information 
        respData = sprintf('%i\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f',...                              % in results, cells 37:43
            [spaKey resX, resY, resX_dva, resY_dva, tResDur]);               

        % get information about how timing of frames worked
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [15 x trialData %i, 5 x timeData %i, 7 x respData %i, 2 x frameData %i]
        data = sprintf('%s\t%s\t%s\t%s\t%s',condData, stimData, timeData, respData, frameData);
end
