function par = getGaborPars(num)
%
% 2016 by Martin Rolfs

global visual

if nargin < 1
    num = 1;
end

% convenience wrapper function for easy pickings
paren = @(x, varargin) x(varargin{:});

% get gabor sizes
[~, sizes] = getGaborSizes;

for n = 1:num
    par.amp(n) = 1;                         % amplitude; -amp/2:+amp/2, so 1 means full contrast
    par.frq(n) = 4;                         % spatial frequency [cycles/deg]
    par.ori(n) = 0.0*pi;                    % orientation [radians] - 0 and pi are vertical, pi/2 and 3*pi/2 are horizontal // rad2deg(0.0*pi + (n-1)*pi/4) for continuous rotation
    par.pha(n) = 0.0*pi;                    % phase [radians]
    par.sig(n) = paren(sizes/8,n);          % std.dev. of Gaussian envelope [deg]
    par.siz(n) = paren(sizes,n);            % extent of the stimulus from center to border [deg]
    par.asp(n) = 1;                         % aspect ratio of x vs y
    
    % transform to pixels
    par.frqp(n) = par.frq(n)/visual.ppd;
    par.sigp(n) = par.sig(n)*visual.ppd;
    par.sizp(n) = ceil(par.siz(n)*visual.ppd);
    par.sizp(n) = par.sizp(n) + ~mod(par.sizp(n),2);
end
