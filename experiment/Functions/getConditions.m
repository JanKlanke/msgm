function [blor] = getConditions
% 2019 by Jan Klanke

default = [1 2 3];
blor = NaN;

% ask for conditions to be displayed
while length(intersect(blor, default)) < length(blor)
    blor = input('>>>> Enter condition numbers:  ','s');
    if length(blor) >= 1
        blor = str2num(blor);
    elseif isempty(blor)
        blor = default;
    end
   
    if length(intersect(blor, default)) < length(blor)
        fprintf(1, 'WARNING:  At least one of the conditions you specified does not exist. \nWARNING:  You can choose between conditions: %s\n', num2str(default));
    end
end