function drawText(str,x,y)

global scr visual 

bounds = Screen(scr.myimg,'TextBounds',str);

x  = x-bounds(3)/2;     % x position
y  = y-bounds(4)/2;     % y position
c  = visual.fgColor(1); % color

% Screen('Drawtext', scr.myimg, str,x,y,c);
DrawFormattedText(scr.myimg, str, x, y, c);

end