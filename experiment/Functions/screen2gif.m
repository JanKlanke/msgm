function screen2gif(f, filename)

global scr

frame = Screen('GetImage', scr.main);
[imind,cm] = rgb2ind(frame,256);
imwrite(imind,cm,filename,'png');
% if f == 1, imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
% else imwrite(imind,cm,filename,'gif','WriteMode','append'); end