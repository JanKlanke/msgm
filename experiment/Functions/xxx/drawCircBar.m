function drawCircBar(p, onPos, VecXY, width, col, windowptr)
% by Jan Klanke 2019

% Correct for case that the supposed position of the nth dot (re onset) is
% a smaller number than the onset position index
p(p < onPos) = length(VecXY) + p(p < onPos);

% calculate vector of dot indices
p = onPos:p;

% Correct for for dot indices that exceed the number of position in the
% vector
p(p > length(VecXY)) = p(p > length(VecXY)) - length(VecXY);

% create position vector
vec = VecXY(:,p);

Screen('DrawDots', windowptr, vec, width, col, [], 1); % draws clockface by means of 3 pixel dots
end