function sac = getMsData(vpno)
%
% read in microsaccade velocitz profiles from files
% input: vp number
% output: array with data velocity profiles, number of saccade, duration,
% peak velocity, distance, and angle
%
% 2020 by Jan Klanke

msdata.file = dir(sprintf('Data/replayCoords/*%s.csv',vpno));
msdata = importdata(msdata.file.name);
[~, ~, idx] = unique(msdata.data(:,1:4), 'rows');
for i = 1:max(idx)
    sac(i).Coords = msdata.data(idx==i,7:8)';
    sac(i).Vector = msdata.data(idx==i,9:10)';
    sac(i).Veccmb = sqrt(sac(i).Vector(1,:).^2 + sac(i).Vector(2,:).^2);
    
    sac(i).Ampltd = sac(i).Veccmb(end) - sac(i).Veccmb(1);
    sac(i).vpeaks = max(diff(sac(i).Veccmb) ./ 2 * 1000 * (1440 / 500));
    sac(i).Duratn = length(sac(i).Veccmb) * 1/1440 * 1000; 
    sac(i).Angle  = atan2(sac(i).Vector(1,end),sac(i).Vector(2,end));
end

end
