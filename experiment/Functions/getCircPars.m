function pars = getCircPars(circRad, circW, circC )
% 2014 by Martin Rolfs, adapted 2017 by Luke Pendergrass and 2018 by Jan
% Klanke

global visual scr %#ok<NUSED>

% make sure that input in output
pars.radius = circRad;
pars.width  = circW;
pars.color  = circC;

% Appearance Parameters
pars.Cradius = round(circRad * visual.ppd);      % circle radius [dva]
pars.circW   = round(circW   * visual.ppd);      % width of clockhand [dva]

% Speed parameters
pars.Speed = scr.refr * 2;                  % clock speed as points on track, scr.refr * 2 := clockhand traverses half the clock face per second  
pars.speedpDeg = 360 / (pars.Speed * (scr.fd * 1000));  % clock speed time per degree [ms] 

% Report Options
pars.reportOpt = pars.Speed;

% clock spatial parameter calculations
anglesDegr = linspace(0, 360, pars.Speed + 1);
anglesDegr(pars.Speed + 1) = [];
anglesRadr = anglesDegr * (pi / 180);
VectorXunshifted = cos(anglesRadr) .* pars.Cradius;
VectorYunshifted = sin(anglesRadr) .* pars.Cradius;
VectorX = circshift(VectorXunshifted, [(round(length(VectorXunshifted) / 4)),...
    (round(length(VectorYunshifted) / 4))]);
VectorY = circshift(VectorYunshifted, [(round(length(VectorYunshifted) / 4)),...
    (round(length(VectorYunshifted) / 4))]);

% combine x and y coordinates of clock hand and -report hand vectors into
% one
pars.pos = [VectorX; VectorY];   

end