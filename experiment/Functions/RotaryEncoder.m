classdef RotaryEncoder < handle
    
    properties
        arduino
    end
    
    methods
        
        function self = RotaryEncoder(port)
            self.connect(port);
        end
        
        function connect(self, port)
            % Check whether ACM port is recognized under 'port'
            if isunix
                avaSP = instrhwinfo('serial');
                fprintf(1, 'Checking availability of ACM port under %s ... \n', port)
                if ~any(strcmp(avaSP.AvailableSerialPorts, port))
                    fprintf(1, 'Matlab currently fails to recognize Arduino''s port.\n To change that and create a symlink pls enter the pw below \n:');
                    system(['sudo ln -s /dev/ttyACM0 ', port]);        
                end
                fprintf(1, 'Port available. \n');
            end
            
            % Configure serial port
            self.arduino = serial(port, 'BaudRate', 115200);
            disp(['Trying to connect to Arduino at "', port, '"...']);
            try
                fopen(self.arduino);
            catch ME
                disp('Connection could not be established.');
                rethrow(ME);
            end
            % Wait for serial interface of Arduino to be ready
            WaitSecs(1);
            disp('Connection established.');
        end
        
        function disconnect(self)
            fclose(self.arduino);
        end
        
        function delete(self)
            self.disconnect();
        end
        
        function requestStatus(self)
            fprintf(self.arduino, 1);
        end
        
        function [encoder, button] = receiveStatus(self)
            data = fscanf(self.arduino, '%d %d');
            encoder = data(1);
            button= data(2);
        end
        
        function [encoder, button] = getStatus(self)
            self.requestStatus();
            [encoder, button] = self.receiveStatus();
        end
        
        function resetEncoder(self)
            fprintf(self.arduino, 2);
        end
        
    end
end

