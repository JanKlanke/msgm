function prepStim
%
% 2008 by Martin Rolfs

global visual scr setting


visual.black = BlackIndex(scr.main);
visual.white = WhiteIndex(scr.main);

visual.bgColor = (visual.black+visual.white)/2;     % background color
visual.fgColor = visual.black;                      % foreground color
visual.inColor = visual.white-visual.bgColor;       % increment for full contrast

visual.ppd     =  scr.ppd;   % pixel per degree

visual.scrCenter = [scr.centerX scr.centerY scr.centerX scr.centerY];


visual.fixCkRad = round(1.5 * visual.ppd); % org: round(1.5*visual.ppd); % fixation check radius
visual.fixCkCol = visual.black;    % fixation check color

% boundary radius at saccade target
visual.boundRad = visual.fixCkRad;

visual.fNyquist = 0.5;


% create standard gabor
nStim = setting.stimC * setting.stimN;
visual.procGaborPar = getGaborPars(nStim);
sizp = unique(visual.procGaborPar.sizp, 'stable');
for i = 1:setting.stimC
    visual.procGaborTex(i) = CreateProceduralGabor(scr.myimg, sizp(i), sizp(i), 0, [visual.bgColor visual.bgColor visual.bgColor 0],1,visual.inColor);
end

% get priority of window activities to maximum
if IsLinux
    scr.priorityLevel = 1;
else
    scr.priorityLevel = MaxPriority(scr.main);
end
