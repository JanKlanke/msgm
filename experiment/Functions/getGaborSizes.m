function [r, s] = getGaborSizes(nring, nstim) 

% Apr. 2020 by Jan Klanke

global setting scr

if nargin < 1
    nring = setting.stimC;
    nstim = setting.stimN;
end

% calculate max size for cirlces to be placed in 
rmax = pix2dva(min(scr.resX/2, scr.resY/2), scr);     % max dva you have for stim placement [dva]

% loop over rings
for i = 1:nring
    rall = 1:0.001:rmax; uall = 2*pi*rall/2/nstim;          % calculate vectors with all possible rad. length and stim. circumfences [dva]
    r(i) = rall(find(uall < rmax - rall, 1, 'last'));       % calculate max radius for which all stim. fit within circle
    s(i) = rmax - r(i);                                     % calculate radius of stimulus (i.e. size)
    rmax = rmax - 2*s(i);                                   % update max radius of cirlce
end

% round
r = floor(4 * r) / 4;         % round down to nearest .25
s = floor(4 * s) / 4 * 2;     % round down to nearest .25 AND change from radius to DIAMETER!!

% duplicate to no. of stims.
r= repelem(r, 1, nstim);
s= repelem(s, 1, nstim);