---
title: "Script to calculate the stimulus positions during the microsaccadic replay."
author: Jan Klanke
date: "`r Sys.Date()`"
output: 
  pdf_document: default
---


```{r setup, include= FALSE}
knitr::opts_chunk$set( include= TRUE, size= 'footnotesize', message=FALSE, warning=FALSE )
knitr::opts_knit$set(  root.dir= '/Users/jan/Documents' )

# Change hook to be able to modify font size in chunks
def.source.hook <- knitr::knit_hooks$get( 'source' )
knitr::knit_hooks$set( source= function( x, options ) {
  x <- def.source.hook( x, options )
  ifelse( !is.null( options$size ), 
         paste0( '\\', options$size, '\n\n', x, '\n\n \\normalsize' ), 
         x ) } )
```

# Parameters
Here go all the parameters needed for this script: i.e. column names, etc.
```{r}
# Names the columns with VP number, session number, block number, and trial number.
identifier <- c( 'vpno', 'seno', 'block', 'trial' )

# Which saccade do you want to look at?
nsac <- 32

# Important eye parameter
trackFreq <- 500
sacMinDur <- 5
sacMaxAmp <- 1

# Specify parameter for fitting of RMSE procedure
rmse <- list()    # - list for RMSEs per saccade
granLvl <- 50     # - how extensive is the grid supposed to be?

# Do you want to rerun all the data
reEvalAll <- 'no'
statusUpdate <- 'yes'
eye <- 'L'
```

# Folder & format spec. parameter
Variables for path to data folder and format specifications. Don't touch unless you use you own folder structure.
```{r}
# Directory of subfolder with data. 
folder_data <- './dataMSGM/preprocessed/msCoordinates'

# Format specs of input and output data (likely no need for change!).
fSpec_csv <- '.csv'  # file format of input/output data
``` 


# Load libraries & sources
Don't touch! These are the libraries that you need in the following and the script does not really work without them (surprise!).
```{r results='hide', message=FALSE, warning=FALSE}
# Load libraries.
libraries <- c( 'data.table', 'vroom', 'zoo', 'tidyverse', 'Cairo', 'RColorBrewer' )
lapply( libraries, require, character.only= TRUE )
```

# Load files
Check which files are in the data folder, and list all denotged 'raw' with the correct format spec: '.csv'. Load all listed files in that folder by row. Also: have a first cursory glance on the data.
```{r results= 'hide', message= FALSE, warning= FALSE, fig.align= "center" }
# Get files in target folder.
fList <- list()
list.files( folder_data, paste0( '*', eye, '_raw', fSpec_csv ), full.names= TRUE ) -> fList$raw
list.files( folder_data, paste0( '*processed*', fSpec_csv ) ) %>% str_remove( ., '_[^_]+$') -> fList$processed

if( reEvalAll != 'yes' && !is_empty( fList$processed ) ) {
  fList$raw <- fList$raw[ !grepl( paste( str_remove( fList$processed, '_([^_]+)' ), collapse= '|' ), fList$raw ) ] }
  
# Load data and group by relevant columns.
vroom( fList$raw, col_names= TRUE, delim= ',' ) %>% 
  unite( sID, c( tID, sacNumber ) ) %>% 
  group_by( sID ) %>% 
  filter( flag == 'saccade' ) %>%
  mutate_at( vars( contains( 'Idx' ) ), list(~ . - .[1] ) ) -> msxy_raw

# Calculate the velocities in the different dimension (horizontal vs. vertical).
mutate_at( msxy_raw, vars( contains( 'coor' ) ), list( vel= ~coalesce( ( . - lag(.) ) * trackFreq, 0 ) ) ) %>%
  rename_at( vars( contains( 'vel' ) ), list( ~c( 'velX', 'velY' ) ) ) -> vel_raw

# Testwise plot of the data
filter( vel_raw, sID %in% distinct( msxy_raw, sID )[nsac,]$sID ) -> df 
ggplot( df, mapping= aes( x= coorX, y= coorY ) ) + geom_path() + geom_point( color= 'red' ) +
  xlab( 'Hor. gaze position [dva]' ) + ylab( 'Vert. gaze position [dva]' ) + facet_wrap( vars( sID ) ) +
  coord_equal() + theme_minimal() -> p1
ggplot( df, mapping= aes( x= msecIdx, y= velX ) ) + geom_path() + geom_point( color= 'red' ) +
  xlab( 'Time [ms]' ) + ylab( 'Hor. velocity [dva/s]' ) +
  theme_minimal() -> p2
ggplot( df, mapping= aes( x= msecIdx, y= velY ) ) + geom_path() + geom_point( color= 'red' ) +
  xlab( 'Time [ms]' ) + ylab( 'Vert. velocity [dva/s]' ) +
  theme_minimal() -> p3
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p3 ), 
                         widths= c( 1.5, 1 ), layout_matrix = rbind( c( 1, 2 ) , c( 1, 3 ) ) )
```

# Normalize gaze positions
Center saccades on 0 by subtracting the first coordinate pair from all tbat follow.
```{r fig.align= "center"}
# Normalize saccades.
mutate_at( msxy_raw, vars( contains( 'coor' ) ), list( ~ . - .[1] ) ) -> msxy_norm

# Calculate the velocities in the different dimension (horizontal vs. vertical).
mutate_at( msxy_norm, vars( contains( 'coor' ) ), 
           .funs= list( vel= ~coalesce( ( . - lag(.) ) * trackFreq, 0 ) ) ) %>%
  rename_at( vars( contains( 'vel' ) ), list( ~c( 'velX', 'velY' ) ) ) -> vel_norm

# Calcualte plot specifics for x and y achsis length.
filter( vel_norm, sID %in% distinct( vel_norm, sID )[nsac,]$sID ) -> df 
ggplot( df, mapping= aes( x= coorX, y= coorY ) ) + geom_path() + geom_point( color= 'red' ) +
  xlab( 'Hor. gaze position [dva]' ) + ylab( 'Vert. gaze position [dva]' ) +
  facet_wrap( vars( sID ) ) + coord_equal() + theme_minimal() -> p1
ggplot( df, mapping= aes( x= msecIdx, y= velX ) ) + geom_path() + geom_point( color= 'red' ) +
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Hor. velocity [dva/s]' ) +
  theme_minimal() -> p2
ggplot( df, mapping= aes( x= msecIdx, y= velY ) ) + geom_path() +geom_point( color= 'red' ) +
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Vert. velocity [dva/s]' ) +
  theme_minimal() -> p3
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p3 ), 
                         widths= c( 1.5, 1 ), layout_matrix = rbind( c( 1, 2 ) , c( 1, 3 ) ) )
```

# Filter by amplitude
Cut off those samples of the microsaccade that follow after the maximal amplitude is reached in either(!) the x or y dimension.
```{r fig.align= "center"}
# Kick out ms sample after max amplitde is reached
group_by( vel_norm, sID ) %>% 
  slice( 1: min( c( which.max( abs( coorX ) ), which.max( abs( coorY ) ) ) ) ) %>%
  mutate_at( vars( matches( c( 'coorX', 'coorY' ) ) ), list( maxA= ~last(.) - first(.) ) ) %>%                                                   # - recalculate max amplitude of combined 
  rename_at( vars( contains( 'maxA' ) ), list( ~c( 'maxAmpX', 'maxAmpY' ) ) ) -> vel_ampFiltered                                                 # - rearrange to make tibble more compellin
 
# Plot stuff.
nsac <- 34
filter( vel_ampFiltered, sID %in% distinct( vel_ampFiltered, sID )[nsac,]$sID ) -> df
ggplot( df, mapping= aes( x= coorX, y= coorY ) ) + geom_path() + geom_point( color= 'red' ) +
  xlab( 'Hor. gaze position [dva]' ) + ylab( 'Vert. gaze position [dva]' ) +
  facet_wrap( vars( sID ) ) + coord_equal() + theme_minimal() -> p1
ggplot( df, mapping= aes( x= msecIdx, y= velX ) ) + geom_path() + geom_point( color= 'red' ) +
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Hor. velocity [dva/s]' ) +
  theme_minimal() -> p2
ggplot( df, mapping= aes( x= msecIdx, y= velY ) ) + geom_path() + geom_point( color= 'red' ) +
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Vert. velocity [dva/s]' ) +
  theme_minimal() -> p3
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p3 ), 
                         widths= c( 1.5, 1 ), layout_matrix = rbind( c( 1, 2 ) , c( 1, 3 ) ) )
```

# Filter saccades that are too short.
If, by amplitude filtering, the remaining saccades shorter than 5 ms, they need to be kicked to the curb.
```{r}
# Filter out saccades that are too short or exceed max amplitude.
group_by( vel_ampFiltered, sID ) %>% 
  filter( sum( flag == 'saccade' ) > ceiling( sacMinDur / ( 1000 / trackFreq ) ) ) -> vel_lenFiltered
```

# Project real eye positions on saccade vector
Calculate the relative position of each sample during the saccade relative to the saccade amplitude. It might be easier to think of this as the percentage 'copmpleted' of the microsaccade (eye position in each sample rel. microsaccade length). In a second step, project these relative position values on the artificially created, 'simplified' saccade vector. Values for the saccade vector are plotted in blue and with dashed line in the following.
```{r fig.align= "center"}
rowwise( vel_lenFiltered ) %>%                                                                                                        # - as as grouping by sID, sac number, AND msec Id  
  mutate( relPos= c( coorX, coorY ) %*% c( maxAmpX, maxAmpY ) / sqrt( ( maxAmpX )^2 + ( maxAmpY )^2 )^2,                              # - calculate 'coordinate / full vector' (i.e. % done)
          coorPX= relPos %*% ( maxAmpX ), coorPY= relPos %*% ( maxAmpY ) ) %>%                                                        # - calculate simpülified X and Y coordinates
  group_by( sID ) %>% 
  mutate_at( vars( matches( c( 'coorPX', 'coorPY' ) ) ), 
             list( velP= ~coalesce( ( . - lag(.) ) * trackFreq , 0 ) ) ) %>%
  rename_at( vars( contains( 'velP' ) ), list( ~c( 'velPX', 'velPY' ) ) ) -> vel_projected

# Plot stuff.
nsac= 20
filter( vel_projected, sID %in% distinct( vel_projected, sID )[nsac,]$sID ) -> df 
ggplot() + geom_path( df, mapping= aes( x= coorX, y= coorY ) ) + 
  geom_point( df, mapping= aes( x= coorX,  y= coorY ), color= 'red' )  + 
  geom_path( df, mapping= aes( x= coorPX, y= coorPY ), linetype= 'dashed' ) +
  geom_point( df, mapping= aes( x= coorPX, y= coorPY ), color= 'blue' ) +
  xlab( 'Hor. gaze position [dva]' ) + ylab( 'Vert. gaze position [dva]' ) +
  facet_wrap( vars( sID ) ) + coord_equal() + theme_minimal() -> p1
ggplot() + geom_path( df, mapping= aes( x= msecIdx, y= velX ) ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velX ), color= 'red' ) +
  geom_path( df, mapping= aes( x= msecIdx, y= velPX ), linetype= 'dashed' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velPX ), color= 'blue' ) + 
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Hor. velocity [dva/s]' ) +
  theme_minimal() -> p2
ggplot() + geom_path( df, mapping= aes( x= msecIdx, y= velY ) ) + 
  geom_point(df, mapping= aes( x= msecIdx, y= velY ), color= 'red' ) +
  geom_path( df, mapping= aes( x= msecIdx, y= velPY ), linetype= 'dashed' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velPY ), color= 'blue' ) + 
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Vert. velocity [dva/s]' ) +
  theme_minimal() -> p3
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p3 ), 
                         widths= c( 1.5, 1 ), layout_matrix = rbind( c( 1, 2 ) , c( 1, 3 ) ) )
```

# Fitting a gamma function to the velocity profile of the sacccade vector
In the following chunk, a gamma function is fitted to the velocity values of the projected(!) 1D saccade vector. The fitting is done as a grid search of the parameterspace alpha: 0-100, beta: .1-5, gamma: 1-20, by means of a grid search. Optimal fits are determined by means of a root mean square error (RMSE) procedude. Fitted velocity curves of 5 exemplary trials are depicted in green and dotted lines in the following, as are the RMSE values for these trials.
```{r fig.align= "center"}
# Specifiy as function to be fit: GAMMA.
gamma_function <- function( t, alpha, beta, gamma ) {
    alpha * ( t / beta )^( gamma - 1 ) * exp( - t / beta ) 
}

# Prepare data for RMSE: calculate 1D gaze position vector for real and projected saccade data (dva)
# and calcualte the velocities along these two vectors (dva/s).
mutate( vel_projected, 
        coorXY= sqrt( coorX^2 + coorY^2 ), coorPXY= sqrt( coorPX^2 + coorPY^2 ), 
        velXY= coalesce( ( coorXY - lag( coorXY ) ) / 2 * 1000, 0 ), 
        velPXY= coalesce( ( coorPXY - lag( coorPXY ) ) / 2 * 1000, 0 ) ) -> vel_model

# Create matrix with parameter combinations for grid search.
tibble( alpha= seq( 0, 100, length.out= granLvl ),
        beta=  seq( .1, 5, length.out= granLvl ),
        gamma= seq( 1, 20, length.out= granLvl ) ) %>% 
  expand.grid() -> parameterGrid

# Loop over saccades to calculate RMSE for each set of parameters per saccade.
for( nsacl in 1: nrow( distinct( vel_model, sID ) ) ) {
  
  # Extract individual saccades.
  filter( vel_model, sID == distinct( vel_model, sID )[nsacl,]$sID ) -> df 
    
  # Reshape parameter matrix and time data into sIDy tibble.
  tibble( msecIdx= rep( df$msecIdx, nrow( parameterGrid ) ), 
          alpha= rep( parameterGrid$alpha, each= length( df$msecIdx ) ), 
          beta= rep( parameterGrid$beta, each= length( df$msecIdx ) ),
          gamma= rep( parameterGrid$gamma, each= length( df$msecIdx ) ) ) %>%
    mutate( velMXY= gamma_function( msecIdx, alpha, beta, gamma ) ) -> df_model
  
  # Do the rmse procedure.
  df[ rep( seq_len( nrow( df ) ), nrow( parameterGrid ) ), ] %>%
    bind_cols( df_model ) %>% 
    mutate( error= ( velMXY - velPXY ) ) %>%# - importantly: Calculate difference between modelled and PROJECTED(!) data 
    group_by( alpha, beta, gamma ) %>% 
    summarize( sID= unique( sID ), rmse= sqrt( sum( error^2 ) / nrow( df ) ) ) -> rmse[[ nsacl ]]
  
  # Counter 
  otip <- 100 / distinct( vel_model, sID ) %>% nrow()
  if( statusUpdate == 'yes' && round( nsacl * otip ) %% 5 == 0 && !( round( ( nsacl * otip ) - otip ) %% 5 == 0 ) ) {
    print( sprintf( '%s: %.2f %% of the saccades are fitted.', format( Sys.time(), '%X' ), round( nsacl * otip ) ) )
  }
}

# Recombine data.
bind_rows( rmse ) %>% group_by( sID ) %>% filter( rmse == min( rmse ) ) %>% 
  left_join( vel_model, ., by= 'sID' ) %>% 
  mutate( velMXY= gamma_function( msecIdx, alpha, beta, gamma ) ) -> vel_fit_gamma

# Plot stuff.
nsacS <- 20
filter( vel_fit_gamma, sID %in% distinct( vel_fit_gamma, sID )[nsacS,]$sID ) -> df
ggplot( df ) + geom_path( mapping= aes( x= coorX, y= coorY ) ) + 
  geom_point( mapping= aes( x= coorX, y= coorY ), color= 'red' ) + 
  geom_point( mapping= aes( x= coorPX, y= coorPY ), color= 'blue' ) +
  geom_path( mapping= aes( x= coorPX, y= coorPY ), linetype= 'dashed' ) +
  xlab( 'Hor. gaze position [dva]' ) + ylab( 'Vert. gaze position [dva]' ) +
  facet_wrap( vars( sID ), ncol= 1, scale= 'fixed' ) + coord_fixed() + 
  theme_minimal() -> p1 
ggplot( df ) + geom_path( mapping= aes( x= msecIdx,  y= velXY ) ) + 
  geom_point( aes( x= msecIdx,  y= velXY ), color= 'red' ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velPXY ), linetype= 'dashed' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velPXY ), color= 'blue' ) + 
  geom_path( aes( x= msecIdx,  y= velMXY ), linetype= 'dotted' ) +
  geom_point( aes( x= msecIdx,  y= velMXY ), color= 'green' ) +
  xlab( 'Time [ms]' ) + ylab( 'Velocity along 1D sac. vec. [dva/s]' ) +
  facet_wrap( vars( sID ), ncol= 1 ) + theme_minimal() -> p2
ggplot( df, aes( x= rmse, factor( sID, level= sort( unique( df$sID ), decreasing= TRUE ) ) ) ) +
  geom_point( color= 'green' ) + 
  xlim( 0, max( vel_fit_gamma$rmse ) ) + 
  theme_minimal() + theme( axis.title.y= element_blank() ) -> p_rmse
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p_rmse ), ncol= 3 )
```

# Check RSME and filter for terrible 
In the following chunk, the RMSEs of all fits are compared and fits that are too bad (i.e. RMSE > mean RMSE + 2 * standard deviation ) are excluded. In the end, all RMSEs are ploted. To get an idea whether the exclusion(s) was(were) necessary, the EXCLUDED fits will be displayed as well.
```{r}
# Calculate crucial RSME stats and filter by them.
mean( vel_fit_gamma$rmse ) -> meanRMSE
sd( vel_fit_gamma$rmse )   -> stdeRMSE
meanRMSE + stdeRMSE * 3    -> cutoffCriterion
filter( vel_fit_gamma, rmse < cutoffCriterion ) -> vel_rsmeFltrd

# Plot stuff.
filter( vel_fit_gamma, rmse > cutoffCriterion ) -> df 
brewer.pal( name= 'Greys', 9 ) -> exCol
brewer.pal( name= 'Spectral', 11 ) -> bgCol
ggplot() + 
  geom_rect( aes( xmin= -Inf, xmax= Inf, ymin= meanRMSE - stdeRMSE * 3, ymax= meanRMSE + stdeRMSE * 3 ), fill= exCol[9], alpha= .5 ) +
  geom_rect( aes( xmin= -Inf, xmax= Inf, ymin= meanRMSE - stdeRMSE * 2, ymax= meanRMSE + stdeRMSE * 2 ), fill= exCol[4.5], alpha= .5 ) +
  geom_rect( aes( xmin= -Inf, xmax= Inf, ymin= meanRMSE - stdeRMSE, ymax= meanRMSE + stdeRMSE), fill= exCol[1], alpha= .5 ) +
  geom_hline( yintercept= meanRMSE, linetype= 'dashed' ) +
  geom_hline( yintercept= cutoffCriterion, color= 'red' ) + 
  geom_label( aes( x= unique( vel_fit_gamma$sID )[length( unique( vel_fit_gamma$sID ) ) / 2 ],
                   y= cutoffCriterion, label= 'Cut off crit.' ), color= 'red' ) +
  geom_point( data= vel_fit_gamma, mapping= aes( x= sID, y= rmse, color= rmse ) ) +
  scale_color_distiller( palette= 'Spectral' ) + 
  ylab( 'RMSE [dva/s]' ) +
  theme_minimal() + theme( axis.text.x= element_text( size= 6, angle= 90, hjust= 1, vjust= 1 ), 
                           legend.position= 'none', axis.title.x = element_blank() ) -> p_rmse
ggplot() + 
  geom_rect( df, mapping= aes( xmin= -Inf, xmax= Inf, ymin= -Inf, ymax= Inf, fill= rmse ), alpha= .15 ) +
  scale_fill_distiller( palette= 'Spectral', limits= c( min( vel_fit_gamma$rmse ), max( vel_fit_gamma$rmse ) ) ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velXY ) ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velXY ), color= 'red' ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velPXY ), linetype= 'dashed' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velPXY ), color= 'blue' ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velMXY ), linetype= 'dotted' ) +
  geom_point( df, mapping= aes( x= msecIdx, y= velMXY ), color= 'green' ) +
  xlab( 'Time [ms]' ) + ylab( 'Velocity along 1D sac. vec. [dva/s]' ) +
  theme_minimal() + theme( legend.position= 'none' ) + facet_wrap( vars( sID ), ncol= 1 ) -> p_spec
gridExtra::grid.arrange( ggplotGrob( p_rmse ), ggplotGrob( p_spec ), ncol= 2 )
```

# Recalculate real gaze positions based on the velocity fits
Extract the x- and y- coordinates of the fitted velocities by (a) calculating the cumulative sum of the fitted velocities, (b) dividing these cum. sums by the full sum of the velocities to generate the incremental relative velocity gain, and (c) multiplying the result with the max amplitude of the saccade to be fitted.
NB: In the plot of the gaze coordinates, the projected coordinates are omitted due to the heavy overlap with the modelled values.
```{r fig.align= "center"}
mutate( vel_rsmeFltrd, relVel= cumsum( velMXY ) / sum( velMXY ), 
        coorMX= relVel * maxAmpX, coorMY= relVel * maxAmpY ) %>%
  mutate_at( vars( contains( 'coorM' ) ), list( velMM= ~coalesce( ( . - lag(.) ) / 2 * 1000, 0 ) ) ) %>%
  rename_at( vars( contains( 'velMM' ) ), list( ~c( 'velMX', 'velMY' ) ) ) -> msxy_fit_gamma

filter( msxy_fit_gamma, sID %in% distinct( msxy_fit_gamma, sID )[nsac,]$sID ) -> df
min( ungroup( df ) %>% select( contains( 'vel', ignore.case= FALSE ) ) ) -> vmin
max( ungroup( df ) %>% select( contains( 'vel', ignore.case= FALSE ) ) ) -> vmax
ggplot() + geom_path( df, mapping= aes( x= coorX, y= coorY ) ) + 
  geom_point( df, mapping= aes( x= coorX, y= coorY ), color= 'red' ) + 
  # geom_path( df, mapping= aes( x= coorPX, y= coorPY ), linetype= 'dashed' ) +
  # geom_point( df, mapping= aes( x= coorPX, y= coorPY ), color= 'blue' ) +
  geom_path( df, mapping= aes( x= coorMX, y= coorMY ), linetype= 'dotted' ) +
  geom_point( df, mapping= aes( x= coorMX, y= coorMY ), color= 'green' ) +
  xlab( 'Hor. gaze position [dva]' ) + ylab( 'Vert. gaze position [dva]' ) +
  facet_wrap( vars( sID ), ncol= 1, scale= 'fixed' ) + coord_fixed() + 
  theme_minimal() -> p1
ggplot() + geom_path( df, mapping= aes( x= msecIdx, y= velX ) ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velX ), color= 'red' ) +
  geom_path( df, mapping= aes( x= msecIdx, y= velPX ), linetype= 'dashed' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velPX ), color= 'blue' ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velMX ), linetype= 'dotted' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velMX ), color= 'green' ) + 
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Hor. velocity [dva/s]' ) +
  facet_wrap( vars( sID ), ncol= 1, scale= 'fixed' ) + theme_minimal() -> p2
ggplot() + geom_path( df, mapping= aes( x= msecIdx, y= velY ) ) + 
  geom_point(df, mapping= aes( x= msecIdx, y= velY ), color= 'red' ) +
  geom_path( df, mapping= aes( x= msecIdx, y= velPY ), linetype= 'dashed' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velPY ), color= 'blue' ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velMY ), linetype= 'dotted' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velMY ), color= 'green' ) + 
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  xlab( 'Time [ms]' ) + ylab( 'Vert. velocity [dva/s]' ) +
  facet_wrap( vars( sID ), ncol= 1, scale= 'fixed' ) + theme_minimal() -> p3
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p3 ), 
                         widths= c( 1.5, 1 ), layout_matrix = rbind( c( 1, 2 ) , c( 1, 3 ) ) )
```

# Upsampling mechanism
Because the sampling frequency of the eyetracker differs from the refresh rate of the screen (500 Hz vs 1440 Hz) - the saccade vector needs to be upsampled. This is done by means of the gamma function: Instead of clculating the x and y coordinates for the number of saccade samples (generated by the eye tracker at 500 Hz), the gamma function is used to calcuate the x and y coordinates for the number of screen refreshs for each MS. (In the plots, the real positions and velocities are extrapolated linearly for a better comparison). 
NB: In the plot of the gaze coordinates, the projected coordinates are omitted due to the heavy overlap with the modelled values.
```{r fig.align= "center"}
# Function for upsampling of tracking (500 hz) to display frequency (1440 hz).
upSample <- function( input, cols ) {
  min( input$framIdx ) : max( input$framIdx ) -> fullFrameList                                                                      # - make full list with number of frames
  tibble( sID= rep( input$sID, length.out= length( fullFrameList ) ), framIdx= fullFrameList ) %>%                                  # - make list in a tibble with sID 
    left_join( input, by= c( 'sID', 'framIdx' ) ) -> output                                                                         # - input data and list with trial ID and complete number 
  
  return( output ) 
  }

# Split data, perform function for upsampling per each group and rejoin data.
group_by( msxy_fit_gamma, sID ) %>%
  select( -contains( c( 'relVel', 'coorM', 'velM' ) ) ) %>%
  group_split() %>% map( ., upSample ) %>% bind_rows() %>%
  fill( c( 'sID', all_of( identifier ), 'flag',
           contains( 'max' ), 'alpha', 'beta', 'gamma', 'rmse' ), .direction= 'down' ) %>%
  mutate_at( vars( contains(  c( 'relPos', 'Idx' ) ) ), list( ~zoo::na.approx(.) ) ) %>%
  group_by( sID ) -> vel_pre_upsampled

# Calculate the velocities in the different dimension (horizontal vs. vertical).
group_by( vel_pre_upsampled, sID ) %>% 
  mutate( velMXY= gamma_function( msecIdx, alpha, beta, gamma ),
          relVel= cumsum( velMXY ) / sum( velMXY ), 
          coorMX= relVel * maxAmpX, coorMY= relVel * maxAmpY ) %>%
  mutate_at( vars( contains( 'coorM' ) ), list( velMM= ~coalesce( ( . - lag(.) ) * 1440, 0 ) ) ) %>%
  rename_at( vars( contains( 'velMM' ) ), list( ~c( 'velMX', 'velMY' ) ) ) -> vel_upsampled
  
# Plot stuff.
nsac= 20
filter( vel_upsampled, sID %in% distinct( vel_upsampled, sID )[nsac,]$sID ) -> df
min( ungroup( df ) %>% select( contains( 'vel', ignore.case= FALSE ) ) ) -> vmin
max( ungroup( df ) %>% select( contains( 'vel', ignore.case= FALSE ) ) ) -> vmax
ggplot() + geom_path( filter( df, !is.na( coorX ) ), mapping= aes( x= coorX, y= coorY ) ) +
  geom_point( filter( df, !is.na( coorX ) ), mapping= aes( x= coorX, y= coorY ), color= 'red' ) + 
  # geom_path( df, mapping= aes( x= coorPX, y= coorPY ), linetype= 'dashed' ) +
  # geom_point( df, mapping= aes( x= coorPX, y= coorPY ), color= 'blue' ) +
  geom_path( df, mapping= aes( x= coorMX, y= coorMY ), linetype= 'dotted' ) +
  geom_point( df, mapping= aes( x= coorMX, y= coorMY ), color= 'green' ) +
  facet_wrap( vars( sID ), ncol= 1, scale= 'fixed' ) + coord_fixed() + 
  xlab( 'Hor. gaze position [dva]' ) + ylab( 'Vert. gaze position [dva]' ) +
  theme_minimal() -> p1
ggplot() + geom_path( df[!is.na( df$velX),], mapping= aes( x= msecIdx, y= velX ) ) + 
  geom_point( filter( df, !is.na( velX ) ), mapping= aes( x= msecIdx, y= velX ), color= 'red' ) +
  geom_path( filter( df, !is.na( velPX ) ), mapping= aes( x= msecIdx, y= velPX ), linetype= 'dashed' ) + 
  geom_point( filter( df, !is.na( velPX ) ), mapping= aes( x= msecIdx, y= velPX ), color= 'blue' ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velMX ), linetype= 'dotted' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velMX ), color= 'green' ) + 
  xlab( 'Time [ms]' ) + ylab( 'Hor. velocity [dva/s]' ) + 
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  facet_wrap( vars( sID ), ncol= 1, scale= 'fixed' ) + theme_minimal() -> p2
ggplot() + geom_path( filter( df, !is.na( velY) ), mapping= aes( x= msecIdx, y= velY ) ) + 
  geom_point(filter( df, !is.na( velY ) ), mapping= aes( x= msecIdx, y= velY ), color= 'red' ) +
  geom_path( filter( df, !is.na( velPY ) ), mapping= aes( x= msecIdx, y= velPY ), linetype= 'dashed' ) + 
  geom_point( filter( df, !is.na( velPY ) ), mapping= aes( x= msecIdx, y= velPY ), color= 'blue' ) + 
  geom_path( df, mapping= aes( x= msecIdx, y= velMY ), linetype= 'dotted' ) + 
  geom_point( df, mapping= aes( x= msecIdx, y= velMY ), color= 'green' ) + 
  xlab( 'Time [ms]' ) + ylab( 'Vert. velocity [dva/s]' ) + 
  ylim( min( ungroup(df) %>% select( contains( 'vel' ) ) ) ,
        max( ungroup(df) %>% select( contains( 'vel' ) ) ) ) +
  facet_wrap( vars( sID ), ncol= 1, scale= 'fixed' ) + theme_minimal() -> p3
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p3 ), 
                         widths= c( 1.5, 1 ), layout_matrix = rbind( c( 1, 2 ) , c( 1, 3 ) ) )
```


# Prepare file for output
Bring data into proper shape for output files. If necessary, convert sID back into meaningful columns. Store the processed data as a csv in a special subfolder.
```{r}
# Split sID into more informative columns.
separate( vel_upsampled, col= sID, into= c( 'tID', 'sacNumber' ), sep= '_' ) %>% 
  separate( col= tID, into= c( identifier ), sep= c( 1, 3, 5 ), remove= F ) -> vel_upsampled

# Split data frame with all results  into different files: one per vp and session.
group_by( vel_upsampled, vpno, seno ) %>% group_split() -> vel_final 

# Create filename.
paste0( folder_data, '/', unique( vel_upsampled$vpno ), unique( vel_upsampled$seno, na.rm= T ), '_gL', granLvl, '_', eye, '_processed', fSpec_csv ) -> filenames

# Save output.
map2( .x= vel_final, .y= filenames,  .f= ~vroom_write( x= .x, path= .y, delim= ',', col_names= TRUE ) )
```