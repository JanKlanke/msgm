#-------------------------------------------------
# Compute inverse discrete Fourier transformation 
# with shift zero frequency to center
# 
# 2012, Ralf Engbert
#-------------------------------------------------

fftsh <- function(x) {
  N <- length(x)
  n <- (N-1)/2
  xt <- c(x[(n+2):N],x[1:(n+1)])
  return(xt)
}